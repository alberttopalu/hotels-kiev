<meta charset="utf-8">

<title> Главная </title>
<meta name="description" content="">

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<meta property="og:image" content="path/to/image.jpg">
<link rel="icon" href="img/favicon/favicon.ico">
<link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-touch-icon-180x180.png">
<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700&display=swap" rel="stylesheet">


<link href="{{ asset('css/main.min.css') }}" rel="stylesheet">

