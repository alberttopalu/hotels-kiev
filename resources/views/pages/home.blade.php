@extends('layouts.default')
@section('content')

    <style>
        .slidecontainer {
            width: 100%;
            float: right !important;
            margin-top: -50px;
        }

        .slider {
            -webkit-appearance: none;
            width: 100%;
            height: 10px;
            border-radius: 5px;
            background: #d3d3d3;
            outline: none;
            opacity: 0.7;
            -webkit-transition: .2s;
            transition: opacity .2s;
        }

        .slider:hover {
            opacity: 1;
        }

        .slider::-webkit-slider-thumb {
            -webkit-appearance: none;
            appearance: none;
            width: 23px;
            height: 24px;
            border: 0;
            /*background: url('contrasticon.png');*/
            background: #4CAF50;
            cursor: pointer;
        }

        .slider::-moz-range-thumb {
            width: 23px;
            height: 24px;
            border: 0;
            /*background: url('contrasticon.png');*/
            background: #4CAF50;
            cursor: pointer;
        }
    </style>

    <section id="content">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="form-group col-md-6 ">
                        <label for="exampleFormControlSelect1">select</label>
                        <select class="form-control" id="exampleFormControlSelect1">
                            @foreach($csv as $item)
                                <option> {{$item[0]}} </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="slidecontainer col-md-6">
                        <input type="range" min="30" max="100" value="50" class="slider" id="myRange">
                        <p>Value: <span id="demo"></span></p>
                    </div>


                    <div class="response col-md-6" id="result">

                    </div>


                    <script>
                        var slider = document.getElementById("myRange");
                        var output = document.getElementById("demo");
                        output.innerHTML = slider.value;

                        slider.oninput = function () {
                            output.innerHTML = this.value;

                            $.ajax({
                                url: "/data",
                                method: "post",
                                data: {
                                    "_token": "{{ csrf_token() }}",
                                    "value": this.value
                                },

                                success: function (arr) {
                                    $('#result').html(arr);
                                }
                            });
                        }
                    </script>
                </div>
            </div>
        </div>
    </section>
@stop