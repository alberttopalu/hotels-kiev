<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class InformationsController extends Controller
{
    public function index(Request $request)
    {
        $range = $request['value'];

        $file = public_path('places.csv');

        $csv = array_map('str_getcsv', file($file));


        $arr = [];
        foreach ($csv as  $key=>$values){

            if ((int)$values[1] >= $range){
                $arr[] = $key = $values;
            }
        }

        return view('pages/home', compact('csv', 'arr'));
    }

    public function data(Request $request){
        $range = $request['value'];

        $file = public_path('places.csv');

        $csv = array_map('str_getcsv', file($file));

        $arr = [];
        foreach ($csv as  $key=>$values){

                if ((int)$values[1] >= $range){
                    $arr[] = $key = $values;
                }
            }
         return response()->json(json_encode($arr));
      }
}
